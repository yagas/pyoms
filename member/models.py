from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nickname = models.CharField(max_length=50, unique=True, verbose_name="昵称", default="", blank=True)
    avatar = models.ImageField(upload_to='media/avatar', blank=True, verbose_name="头像")

    class Meta:
        verbose_name_plural = verbose_name = "会员信息"

    def __str__(self):
        return self.nickname

    def username(self):
        return self.user.username