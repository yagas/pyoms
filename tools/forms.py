from django import forms
from .models import ShortUrl

class CreateForm(forms.Form):
    target = forms.CharField(label='目标网址', max_length=255)

    def encode(self, length):
        dict_word = list('1234567890abcdefghijklnmopqrstuvwxyzABCDEFGHIJKLNMOPQRSTUVWXYZ')
        dict_len = len(dict_word)
        code = ''

        for word in list(self.cleaned_data['target']):
            number = ord(word)
            index = number % dict_len
            code += dict_word[index]

            if len(code) == length:
                break

        return code

    def save(self):
        record = ShortUrl()
        record.target = self.cleaned_data['target']
        record.short = self.encode(6)
        record.view = 0
        if record.exists() is False:
            record.save()