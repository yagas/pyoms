from django.db import models

class ShortUrl(models.Model):
    short = models.CharField(max_length=255)
    target = models.CharField(max_length=255)
    view = models.IntegerField()

    class Meta:
        verbose_name = '短网址'
        verbose_name_plural = verbose_name

    def exist(self):
        return self.objects.filter(short=self.short).exists()