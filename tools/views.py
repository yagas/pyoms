from django.shortcuts import render
from .forms import CreateForm

def shorturl(request):
    if request.method == 'POST':
        record = CreateForm(request.POST)
        if record.is_valid():
            record.save()

    return render(request, 'tools/shorturl.html')
