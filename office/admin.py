from django.contrib import admin
from .models import Products, Groups, PbOrder, Order
from django.contrib.auth.models import User

@admin.register(Products)
class ProductAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'group', 'name', 'created_at')
    search_fields = ('name',)

    def username(self, obj):
        user = User.objects.get(profile__pk=obj.uid)
        return user.profile.nickname

    def group(self, obj):
        return obj.group_name().name

    username.short_description = '昵称'
    group.short_description = '分组'

    fieldsets = [
        ('基本信息', {'fields': ['name', 'uid', 'gid']})
    ]

@admin.register(Groups)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('id', 'user', 'name')

@admin.register(PbOrder)
class OrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'pid', 'uid', 'title', 'status', 'created_at', 'updated_at')
    ordering = ('-created_at',)

@admin.register(Order)
class VOrderAdmin(admin.ModelAdmin):
    list_display = ('id', 'title', 'updated_at')
    ordering = ('-created_at',)