from django.shortcuts import render
from .models import Products

# Create your views here.
def office(request):
    return render(request, 'office/product/view.html')

def products(request):
    products = Products.objects.all()[:20]
    return render(request, 'office/products.html', {"products": products})

def detail(request, id):
    product = Products.objects.get(pk=id)
    params = {
        "product": product
    }
    return render(request, 'office/product/view.html', params)

def create(request):
    return render(request, 'office/product/addnew.html')

def orders(request):
    return render(request, 'office/orders.html')