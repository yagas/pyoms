# Generated by Django 2.1 on 2024-02-02 02:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('office', '0005_auto_20240202_0856'),
    ]

    operations = [
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255, verbose_name='标题')),
                ('status', models.IntegerField(choices=[(1, '未完成')], default=1, verbose_name='状态')),
                ('created_at', models.DateTimeField(auto_now_add=True, verbose_name='创建时间')),
                ('updated_at', models.DateTimeField(auto_now=True, verbose_name='更新时间')),
            ],
            options={
                'verbose_name': '业务订单',
                'verbose_name_plural': '业务订单',
            },
        ),
    ]
