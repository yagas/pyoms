from django.db import models
from django.contrib.auth.models import User

class Groups(models.Model):
    uid = models.IntegerField('用户编号')
    name = models.CharField('名称', max_length=255)

    def __str__(self):
        return self.name

    def user(self):
        return User.objects.get(pk=self.uid)

    @staticmethod
    def enums(uid):
        groups = Groups.objects.filter(pk=uid).all()
        enums = []
        for group in groups:
            enums.append((group.id, group.name))
        return tuple(enums)

    class Meta:
        verbose_name = '分组'
        verbose_name_plural = verbose_name

class Products(models.Model):
    STATUS_ENABLED = 1
    STATUS_DISABLED = 2
    STATUS_DELETED = 3

    STATUS_CHOICES = [
        (STATUS_ENABLED,'启用'),
        (STATUS_DISABLED,'禁用'),
        (STATUS_DELETED,'删除')
    ]

    uid = models.IntegerField('用户编号')
    gid = models.IntegerField('产品分组', blank=True)
    status = models.IntegerField('状态', choices=STATUS_CHOICES, default=STATUS_ENABLED)
    name = models.CharField('产品名称', max_length=200)
    description = models.CharField('产品描述', max_length=255, blank=True)
    fields = models.TextField('字段', blank=True)
    process = models.TextField('步骤', blank=True)
    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    updated_at = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.name

    def user(self):
        return User.objects.get(profile_pk=self.uid)

    def group_name(self):
        return Groups.objects.get(pk=self.gid)

    class Meta:
        verbose_name = '产品'
        verbose_name_plural = verbose_name

class PbOrder(models.Model):
    STATUS_NORMAL = 1;
    STATUS_PENDDING = 2;
    STATUS_DONE = 3;

    STATUS_CHOICES = [
        (STATUS_NORMAL, '未完成'),
        (STATUS_PENDDING, '进行中'),
        (STATUS_DONE, '已完成')
    ]

    pid = models.IntegerField('产品编号')
    uid = models.IntegerField('用户编号')
    title = models.CharField('标题', max_length=255)
    status = models.IntegerField('状态', choices=STATUS_CHOICES, default=STATUS_NORMAL)
    status_sub = models.IntegerField('完结状态', default=0)
    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    updated_at = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '订单'
        verbose_name_plural = verbose_name

class Order(models.Model):
    STATUS_NORMAL = 1;
    STATUS_CHOICES = [
        (STATUS_NORMAL, '未完成'),
    ]

    title = models.CharField('标题', max_length=255)
    status = models.IntegerField('状态', choices=STATUS_CHOICES, default=STATUS_NORMAL)
    created_at = models.DateTimeField('创建时间', auto_now_add=True)
    updated_at = models.DateTimeField('更新时间', auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = '业务订单'
        verbose_name_plural = verbose_name
