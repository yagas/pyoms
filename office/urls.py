from django.urls import path
from . import views

urlpatterns = [
    path('', views.office, name='office'),
    path('products', views.products, name='products'),
    path('product/view/<int:id>', views.detail, name='product_detail'),
    path('product/create', views.create, name='product_create'),
    path('orders', views.orders, name='orders'),
]